
import setuptools
with open("README.rst") as fh:
    long_description = fh.read()

setuptools.setup(
    name="et_tryton-tools",
    version="0.0.1",
    author="Etrivial",
    author_email="monomono@disroot.org",
    description="Herramientas de Etrivial para Tryton",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://git.disroot.org/etrivial",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Os Independent",
    ],
    python_requires=">=3.0",
    require = "Relative_delta"
)
