from datetime import date
from decimal import Decimal

UNIDAD = 'Unidad'
KILOGRAMO = 'Kilogramo'
QUANTIZE_PRECIO = Decimal('0.0000')

UNIDADES = {
    'Unidad': UNIDAD,
    'Unds': UNIDAD,
    'kg': KILOGRAMO,
    'Kg': KILOGRAMO
}

class Linea():
    producto = None
    unidad = None
    precio = None
    cantidad = None
    total = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

    def fill_line(self, line, Model, config, products=None, uoms=None):
        """Llena un objeto de tryton sale.line con la informacion del presente
           objeto
        """
        line.product = self.get_product(Model, config, products)
        unidad, precio, cantidad = self.get_unit_price_quantity(Model, config, uoms)
        line.unit = unidad
        line.unit_price = precio
        line.quantity = cantidad
        return line

    def get_product(self, Model, config, products=None):
        """Devuelve el producto de tryton"""
        if products:
            return products.get(self.producto)
        Product = Model.get('product.product', config=config)
        product = Product.find([('template.name', '=', self.producto)])
        assert product, f"Producto {self.producto} no existe"
        return product[0]

    def get_unit_price_quantity(self, Model, config, uoms=None):
        """Devuelve unit_price y quantity basado en la informacion y en que el
           total coincida.

           Se halla el limite de digitos decimales de la unidad, se ajusta la
           cantidad a ese valor y se divide el total por la cantidad ajustada
           para hallar el valor unitario.
        """
        Uom = Model.get('product.uom', config=config)
        if uoms:
            uom = uoms.get(UNIDADES.get(self.unidad))
        else:
            uom = Uom.find([('name', '=', UNIDADES.get(self.unidad))])[0]
        digits = uom.digits
        quantize = Decimal('0.' + ('0' * digits)) if digits > 0 else Decimal('0')
        cantidad = Decimal(self.cantidad).quantize(Decimal(quantize))
        #Ajustando valores que son redondeados a 0
        if digits == 0:
            if -1 < self.cantidad < 0:
                cantidad = Decimal(-1)
            elif 0 < self.cantidad < 1:
                cantidad = Decimal(1)

        if self.total and cantidad:
            assert cantidad != 0 or cantidad != Decimal(0)
            precio = (Decimal(self.total) / cantidad).quantize(QUANTIZE_PRECIO)
        else:
            precio = Decimal(self.precio).quantize(QUANTIZE_PRECIO)
        return uom, precio, cantidad

    def obtener_vlist(self, Model=None, config=None, products=None, uoms=None):
        Product = Model.get('product.product', config=config)
        linea = {}
        if products:
            product = products.get(self.producto)
        else:
            product = Product.find([('template.name', '=', self.product)])
        linea['product'] = product.id
        unidad, precio, cantidad = self.get_unit_price_quantity(Model, config,
                                                                uoms)
        linea['unit'] = unidad.id
        linea['quantity'] = cantidad
        linea['unit_price'] = precio
        return linea


class Venta():
    tercero = None
    descripcion = None
    fecha = None
    lineas = None
    tienda = None
    empresa = None
    moneda = None

    tryton_object = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if karg == 'lineas':
                self.lineas = []
                for linea in kwargs['lineas']:
                    self.lineas.append(Linea(**linea))
                continue
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])
        assert self.fecha, "Fecha de venta requerida"
        assert isinstance(self.fecha, (date,)), "La fecha debe ser tipo date"

    def set_tryton_object(self, Model, config, products=None, uoms=None, parties=None):
        """Crea (sin almacenar) un objeto en tryton usando proteus

        Opcionalmente se le puede pasar un diccionario products con los productos, util para disminuir consultas al servidor.
        Opcionalmente se le puede pasar un diccionario uoms con los productos, util para disminuir consultas al servidor.
        Opcionalmente se le puede pasar un diccionario uoms con los productos, util para disminuir consultas al servidor.
        """
        Sale = Model.get('sale.sale', config=config)
        Shop = Model.get('sale.shop', config=config)
        Party = Model.get('party.party', config=config)
        Company = Model.get('company.company', config=config)
        sale = Sale()
        sale.sale_date = self.fecha
        if parties:
            sale.party = parties.get(self.tercero)
        else:
            sale.party = Party.find([('name', '=', self.tercero)])
        if self.descripcion:
            sale.description = self.descripcion
        if self.tienda:
            tienda = Shop.find([('name', '=', self.tienda)])
            if tienda:
                sale.shop = tienda[0].id
            else:
                print(f"Advertencia: no existe la tienda {self.tienda}")
        if self.empresa:
            empresa = Company.find([('party.name', '=', self.empresa)])
            if empresa:
                sale.company = empresa[0].id
            else:
                print(f"Advertencia: no existe la empresa {self.company}")

        for linea in self.lineas:
            line = sale.lines.new()
            line = linea.fill_line(line, Model, config, products, uoms)

        self.tryton_object = sale

    def guardar_tryton(self):
        if self.tryton_object:
            self.tryton_object.save()

    def obtener_vlist(self, Model, config, products=None, uoms=None,
                      parties=None, addresses=None, shops=None,
                      companies=None, currencies=None):
        Shop = Model.get('sale.shop', config=config)
        Party = Model.get('party.party', config=config)
        Company = Model.get('company.company', config=config)
        sale = {}

        if parties:
            party = parties.get(self.tercero)
        else:
            sale['party'] = Party.find([('name', '=', self.tercero)])
        sale['party'] = party.id

        if addresses:
            address = addresses.get(self.tercero)
        else:
            address = party.addresses[0]
        sale['invoice_address'] = address.id
        sale['shipment_address'] = address.id

        if self.descripcion:
            sale['description'] = self.descripcion
        sale['sale_date'] = self.fecha

        if self.tienda:
            if shops:
                tienda = shops.get(self.tienda)
            else:
                tienda = Shop.find([('name', '=', self.tienda)])
        else:
            if shops:
                tienda = shops[list(shops.keys())[0]]
            else:
                tienda = Shop.find()[0]
        sale['shop'] = tienda.id

        if self.empresa:
            if companies:
                empresa = companies.get(self.empresa)
            else:
                empresa = Company.find([('party.name', '=', self.empresa)])[0]
        else:
            if companies:
                empresa = companies[list(companies.keys())[0]]
            else:
                empresa = Company.find()[0]
        sale['company'] = empresa.id

        if self.moneda:
            if currencies:
                moneda = currencies.get(self.moneda)
        else:
            moneda = empresa.currency
        sale['currency'] = moneda.id
        vlist_lineas = []
        for l in self.lineas:
            vlist_lineas.append(
                l.obtener_vlist(Model=Model, config=config, products=products,
                                uoms=uoms))
        sale['lines'] = [(('create'), vlist_lineas)]
        return sale
