"""Producto para importar a Tryton"""

from decimal import Decimal

class Producto():
    """Clase producto"""
    nombre = None
    comprable = None
    vendible = None
    tipo = None
    precio_compra = None
    precio_venta = None
    precios = None
    unidad_de_medida = None
    categorias = None
    categoria_contable = None
    metodo_costo = None
    codigo = None
    identificador = None

    tryton_object = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])
        assert self.nombre, "nombre es obligatorio"
        if isinstance(self.comprable, (str, int)):
            self.comprable = bool(self.comprable)
        if isinstance(self.vendible, (str, int)):
            self.vendible = bool(self.vendible)
        if isinstance(self.tipo, (str,)):
            self.tipo = self.traducir_tipo(self.tipo)
        if isinstance(self.metodo_costo, (str,)):
            self.tipo = self.traducir_metodo_costo(self.metodo_costo)
        if isinstance(self.precio_compra, (str,)):
            self.precio_compra = float(self.precio_compra)
        if isinstance(self.precio_venta, (str,)):
            self.precio_venta = float(self.precio_venta)
        if self.precios:
            for precio in self.precios:
                assert isinstance(precio, PrecioEnLista), "Precios debe ser PrecioEnLista"
        if self.unidad_de_medida:
            assert isinstance(self.unidad_de_medida, (str,)), f"La unidad({self.unidad_de_medida}) debe ser str"
        if self.categorias:
            if isinstance(self.categorias, (str,)):
                    self.categorias = [self.categorias]
            for categoria in self.categorias:
                assert isinstance(categoria, (str,)), f"categoria ({categoria}) debe ser str"
        if self.categoria_contable:
            assert isinstance(self.categoria_contable, (str,)), f"La categoria contable debe ser str"

    def traducir_tipo(self, tipo):
        tipo = tipo.lower()
        traducciones = {
            'goods': 'goods', 'bienes': 'goods',
            'service': 'service', 'servicio': 'service',
            'assets': 'assets', 'activo': 'assets',
        }
        return traducciones[tipo]

    def traducir_metodo_costo(self, metodo_costo):
        """Traducir metodo de costo"""
        metodo_costo = metodo_costo.lower()
        FIXED = 'fixed'
        AVERAGE = 'average'
        traducciones = {
            'fixed': FIXED, 'fijo': FIXED, 'arreglado': FIXED,
            'average': AVERAGE, 'promedio': AVERAGE,
        }
        return traducciones[metodo_costo]

    def set_tryton_object(self, Model, config):
        """Crea objeto tryton que representa este objeto"""
        assert bool(self.unidad_de_medida), f"Se requiere la unidad de medida"
        Plantilla = Model.get('product.template', config=config)
        Category = Model.get('product.category', config=config)
        Uom = Model.get('product.uom', config=config)
        assert self.nombre, "nombre requerido"
        plantilla = Plantilla(name=self.nombre)
        plantilla.purchasable = self.comprable
        plantilla.salable = self.vendible
        plantilla.default_uom, = Uom.find([('name', '=', self.unidad_de_medida)])
        if self.categorias:
            for c in self.categorias:
                category = Category.find([('name', '=', c)])
                if not category:
                    category = [Category(name=c)]
                    category[0].save()
                plantilla.categories.append(category[0])
        if self.categoria_contable:
            category = Category.find([('name', '=', self.categoria_contable),
                                      ('accounting', '=', True)])
            if not category:
                category = [
                    Category(name=self.categoria_contable, accounting=True)]
                category[0].save()
            plantilla.account_category = category[0]
        if self.metodo_costo:
            plantilla.cost_price_method = self.metodo_costo
        if self.precio_venta:
            plantilla.list_price = Decimal(float(self.precio_venta))
        else:
            plantilla.list_price = Decimal(0)
        #variante
        if self.codigo:
            plantilla.products[0].code = self.codigo
        if self.precio_compra:
            plantilla.products[0].cost_price = Decimal(float(self.precio_compra))
        else:
            plantilla.products[0].cost_price = Decimal(0)
        if self.identificador:
            plantilla.products[0].identifiers.new(code=self.identificador)
        self.tryton_object = plantilla

    def guardar_tryton(self):
        """Guarda objeto Tryton"""
        assert self.tryton_object, "No se ha creado la plantilla de tryton"
        self.tryton_object.save()


class ListaPrecio():
    nombre = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

class PrecioEnLista():
    lista_precio = None
    precio_producto = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

