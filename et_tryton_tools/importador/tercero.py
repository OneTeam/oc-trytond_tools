
class MecanismoContacto():
    tipo = None
    valor = None
    
    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

    def get_value(self):
        return {'type': self.tipo,
                'value': self.valor}

class Identificador():
    tipo = None
    codigo = None
    
    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

    def get_value(self):
        return {'type': self.tipo,
                'code': self.valor}

class Direccion():
    nombre = None
    calle = None
    ciudad = None
    departamento = None
    subdivision = None
    pais = None
    country = None
    
    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])

    def get_subdivision_tryton(self, Model, config):
        Country = Model.get('country.country', config=config)
        Sub = Model.get('country.subdivision', config=config)
        if self.ciudad:
            pais = self.pais or 'Colombia'
            country = Country.find([('name', '=', pais)])
            if country:
                if self.departamento:
                    subdivision, = Sub.find([
                        ('name', '=', self.departamento),
                        ('country', '=', country.id)])
                    if subdivision:
                        return (country, subdivision)
                else:
                    return (country, None)
        return (None, None)

    def set_atributes_to_tryton_object(self, tryton_object, Model, config):
        if self.nombre:
            tryton_object.name = self.nombre
        if self.calle:
            tryton_object.street = self.calle
        if self.ciudad:
            tryton_object.city = self.ciudad
        if self.departamento or self.pais:
            country, subdivision = self.get_subdivision_tryton(
                Model=Model, config=config)
            if country:
                tryton_object.country = country
            if subdivision:
                tryton_object.subdivision = subdivision
        return tryton_object


class Tercero():
    nombre = None
    mecanismos_de_contacto = None
    identificadores = None
    direcciones = None
    categorias = None

    tryton_object = None

    def __init__(self, **kwargs):
        for karg in kwargs:
            if hasattr(self, karg):
                setattr(self, karg, kwargs[karg])
        self.ajustar_mecanismos_de_contacto()
        self.ajustar_identificadores()
        self.ajustar_direcciones()

    def ajustar_mecanismos_de_contacto(self):
        if self.mecanismos_de_contacto:
            if isinstance(self.mecanismos_de_contacto, (str,)):
                self.mecanismos_de_contacto = [
                    self.str2mecanismo_contacto(self.mecanismos_de_contacto)]
            if isinstance(self.mecanismos_de_contacto, (MecanismoContacto,)):
                    self.mecanismos_de_contacto = [self.mecanismos_de_contacto]
            mcs = []
            for mc in self.mecanismos_de_contacto:
                if isinstance(mc, (str,)):
                    mcs.append(self.str2mecanismo_contacto(mc))
                elif isinstance(mc, (MecanismoContacto,)):
                    mcs.append(mc)
                else:
                    assert True, f"Metodo de contacto ({mc}) no compatible"
            self.mecanismos_de_contacto = mcs

    def ajustar_identificadores(self):
        if self.identificadores:
            if isinstance(self.identificadores, (str,)):
                self.identificadores = [
                    self.str2idenfiticador(self.identificadores)]
            identificadores = []
            for i in self.identificadores:
                if isinstance(i, (Identificador,)):
                    identificadores.append(i)
                elif isinstance(i, (str,)):
                    identificadores.append(self.str2idenfiticador(i))
        
    def ajustar_direcciones(self):
        if self.direcciones:
            if isinstance(self.direcciones, (str,)):
                self.direcciones = [
                    Direccion(calle=self.direcciones)]
            direcciones = []
            for d in self.direcciones:
                if isinstance(d, (Direccion,)):
                    direcciones.append(d)
                elif isinstance(d, (str,)):
                    direcciones.append(Direccion(street=d))
            self.direcciones = direcciones

    def str2mecanismo_contacto(self, mecanismo_de_contacto):
        assert isinstance(mecanismo_de_contacto, (str,)), "debe ser texto"
        return MecanismoContacto(valor=mecanismo_de_contacto)

    def str2idenfiticador(self, identificador):
        assert isinstance(identificador, (str,)), "debe ser texto"
        return Identificador(codigo=Identificador)

    def set_tryton_object(self, Model, config):
        Party = Model.get('party.party', config=config)
        Category = Model.get('party.category')
        party = Party(name=self.nombre)
        if self.mecanismos_de_contacto:
            for mc in self.mecanismos_de_contacto:
                party.contact_mechanisms.new(
                    type=mc.tipo, value=mc.valor)
        if self.direcciones:
            for n, d in enumerate(self.direcciones):
                if n == 0: #usar direccion vacia que se crea al crear el tercero
                    dd = party.addresses[0]
                else:
                    dd = party.addresses.new()
                dd = d.set_atributes_to_tryton_object(dd, Model=Model, config=config)
        if self.identificadores:
            for i in self.identificadores:
                party.identifiers.new(
                    type=i.tipo, code=i.codigo)
        if self.categorias:
            for c in self.categorias:
                category = Category.find([('name', '=', c)])
                if not category:
                    category = [Category(name=c)]
                    category[0].save()
                party.categories.append(category[0])
        self.tryton_object = party

    def guardar_tryton(self):
        if self.tryton_object:
            self.tryton_object.save()


        
