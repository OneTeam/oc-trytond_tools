"""
Funciones utiles para realizar una configuracion inicial de tryton,
1. Las funciones que requieren Usar proteus esperan los objetos Model y config
que se deben pasar para evitar proteus como requisito de este paquete.
"""

from datetime import date
from dateutil.relativedelta import relativedelta

def get_cuenta_gastos(codigo, tipo, Model, config):
    Cuenta = Model.get('account.account', config=config)
    CuentaTipo = Model.get('account.account.type', config=config)
    tipo_gasto, = CuentaTipo.find([('name', '=', tipo)])
    cuenta, = Cuenta.find([('code', '=', codigo)])
    cuenta.template_override = True
    cuenta.type = tipo_gasto
    cuenta.save()
    return cuenta

def crear_tipo_cuenta(nombre, statement, nombre_padre, Model, config, empresa=None, existencia=False, ingreso=False, gasto=False, asiento=False):
    CuentaTipo = Model.get('account.account.type', config=config)
    padre, = CuentaTipo.find([('name', '=', nombre_padre)])
    cuentatipo = CuentaTipo(name=nombre,
                statement=statement,
                parent=padre,
                stock=existencia,
                revenue=ingreso,
                expense=gasto,
                assets=asiento)
    cuentatipo.save()
    return cuentatipo

def crear_cuenta(nombre, codigo, tipo, padre, company, Model, config, activa=True,
         cerrada=False, en_balance=True,
         reconcile=False):
    Cuenta = Model.get('account.account', config=config)
    parent, = Cuenta.find([('code', '=', padre)])
    cuenta = Cuenta(name=nombre, code=codigo,
            type=tipo, active=activa,
            closed=cerrada, reconcile=reconcile,
            parent=parent,
            general_ledger_balance=en_balance)
    cuenta.save()
    return cuenta

def crear_cuenta_analitica_root(nombre, codigo, tipo, activa=True):
    CuentaAnalitica = Model.get('analytic_account.account')
    cuentaanalitica = CuentaAnalitica(name=nombre, code=codigo,
                    type=tipo)
    cuentaanalitica.save()
    return cuentaanalitica

def crear_cuenta_analitica(nombre, codigo, padre, tipo, activa=True):
    CuentaAnalitica = Model.get('analytic_account.account')
    print(padre)
    parent, = CuentaAnalitica.find([('code', '=', padre)])
    cuentaanalitica = CuentaAnalitica(name=nombre, code=codigo,
                    parent=parent, type=tipo)
    cuentaanalitica.save()
    return cuentaanalitica

def crear_secuencia(nombre, codigo, prefijo, relleno = 6):
    Secuencia = Model.get('ir.sequence')
    secuencia = Secuencia(name = nombre, 
            code = codigo, prefix=prefijo, padding = relleno)
    secuencia.save()
    return secuencia

def activate_modules(modules, Model, Wizard, config):
    if isinstance(modules, str):
        modules = [modules]
    Module = Model.get('ir.module', config=config)
    records = Module.find([
    ('name', 'in', modules),
    ])
    assert len(records) == len(modules), f"faltan los modulos {set(modules).difference(set(r.name for r in records))}"
    Module.click(records, 'activate')
    Wizard('ir.module.activate_upgrade', config=config).execute('upgrade')

def get_company(Model, config):
    "Return the only company"
    Company = Model.get('company.company', config=config)
    company, = Company.find()
    return company

def create_party(nombre, Model, config, telefonos=None, celulares=None, identificador=None, calle=""):
    Party = Model.get('party.party', config=config)
    party = Party(name=nombre)
    if identificador:
        _ = party.identifiers.new(code=identificador)
    if telefonos:
        for tel in telefonos:
            _ = party.contact_mechanisms.new(type='phone', value=tel)
    if celulares:
        for celular in celulares:
            _ = party.contact_mechanisms.new(type='mobile', value=celular)
    party.save()
    if calle:
        adress, = party.addresses
        adress.street = calle
        adress.save()
    return party

def create_company(party, currency, Model, Wizard, config, timezone=None):
    "Create the company using the proteus config"
    Party = Model.get('party.party', config=config)
    User = Model.get('res.user', config=config)

    company_config = Wizard('company.company.config', config=config)
    company_config.execute('company')
    company = company_config.form
    if not party:
        party = Party(name='Dunder Mifflin')
        party.save()
    company.party = party
    company.currency = currency
    company_config.execute('add')
    config._context = User.get_preferences(True, {})
    return company_config


def create_fiscalyear(Model, config, company=None, today=None):
    "Create a fiscal year for the company on today"
    FiscalYear = Model.get('account.fiscalyear', config=config)
    Sequence = Model.get('ir.sequence', config=config)

    if not company:
        company = get_company(Model, config)

    if not today:
        today = date.today()

    fiscalyear = FiscalYear(name=str(today.year))
    fiscalyear.start_date = today + relativedelta(month=1, day=1)
    fiscalyear.end_date = today + relativedelta(month=12, day=31)
    fiscalyear.company = company

    post_move_sequence = Sequence(
        name=f"Asiento contabilizado {str(today.year)}",
        code='account.move',
        company=company)
    post_move_sequence.save()
    fiscalyear.post_move_sequence = post_move_sequence
    fiscalyear.account_stock_method = 'continental'
    return fiscalyear

def create_chart(
        Wizard, Model, config,
        CUENTA_A_COBRAR, CUENTA_A_PAGAR, TIPO_CUENTA_COMPRAS, CUENTA_COMPRAS_NOMBRE, CUENTA_COMPRAS, CUENTA_COMPRAS_PADRE, CUENTA_INGRESOS,
        company=None, chart='account.account_template_root_en'):
    "Create chart of accounts"
    AccountTemplate = Model.get('account.account.template', config=config)
    CuentaTipo = Model.get('account.account.type', config=config)
    ModelData = Model.get('ir.model.data', config=config)

    if not company:
        company = get_company(Model, config)

    module, xml_id = chart.split('.')
    data, = ModelData.find([
        ('module', '=', module),
        ('fs_id', '=', xml_id),
    ], limit=1)

    account_template = AccountTemplate(data.db_id)

    create_chart = Wizard('account.create_chart', config=config)
    create_chart.execute('account')
    create_chart.form.account_template = account_template
    create_chart.form.company = company
    create_chart.execute('create_account')


    Account = Model.get('account.account')
    receivable, = Account.find([('company', '=', company.id),
                                ('code', '=', CUENTA_A_COBRAR)])
    payable, = Account.find([('company', '=', company.id),
                             ('code', '=', CUENTA_A_PAGAR)])
    tipo_cuenta_compra, = CuentaTipo.find([('name', '=', TIPO_CUENTA_COMPRAS)])
    cuenta_compras = crear_cuenta(
        CUENTA_COMPRAS_NOMBRE,
        CUENTA_COMPRAS,
        tipo_cuenta_compra,
        CUENTA_COMPRAS_PADRE,
        company, Model, config, en_balance=True
    )
    expense = cuenta_compras
    revenue, = Account.find([('company', '=', company.id),
                             ('code', '=', CUENTA_INGRESOS)])
    create_chart.form.account_receivable = receivable
    create_chart.form.account_payable = payable
    create_chart.form.category_account_expense = expense
    create_chart.form.category_account_revenue = revenue
    create_chart.execute('create_properties')
    return create_chart

def set_fiscalyear_invoice_sequences(fiscalyear, Model, config):
    "Set invoice sequences to fiscalyear"
    SequenceStrict = Model.get('ir.sequence.strict', config=config)

    in_invoice_seq = SequenceStrict(
        name=f"Factura Proveedor {fiscalyear.name}",
        code='account.invoice',
        company=fiscalyear.company)
    in_invoice_seq.save()
    in_credit_note_seq = SequenceStrict(
        name=f"Abono Proveedor {fiscalyear.name}",
        code='account.invoice',
        company=fiscalyear.company)
    in_credit_note_seq.save()
    out_invoice_seq = SequenceStrict(
        name=f"Factura Cliente {fiscalyear.name}",
        code='account.invoice',
        company=fiscalyear.company)
    out_invoice_seq.save()
    out_credit_note_seq = SequenceStrict(
        name=f"Abono Cliente {fiscalyear.name}",
        code='account.invoice',
        company=fiscalyear.company)
    out_credit_note_seq.save()

    seq, = fiscalyear.invoice_sequences
    seq.out_invoice_sequence = out_invoice_seq
    seq.in_invoice_sequence = in_invoice_seq
    seq.out_credit_note_sequence = out_credit_note_seq
    seq.in_credit_note_sequence = in_credit_note_seq
    return fiscalyear

def crear_categoria_contable_producto(
        nombre, cuenta_gastos, cuenta_ingresos,
        cuenta_existencia, cuenta_existencia_cliente, cuenta_existencia_proveedor,
        cuenta_existencia_perdido_encontrado, Model, config):
    CategoriaContable = Model.get('product.category', config=config)
    Account = Model.get('account.account', config=config)

    categoria_contable = CategoriaContable(
        name=nombre)
    categoria_contable.accounting = True
    categoria_contable.account_expense = cuenta_gastos
    categoria_contable.account_revenue = cuenta_ingresos
    categoria_contable.account_stock = cuenta_existencia
    categoria_contable.account_stock_lost_found = cuenta_existencia_perdido_encontrado
    categoria_contable.account_stock_customer = cuenta_existencia_cliente
    categoria_contable.account_stock_supplier = cuenta_existencia_proveedor
    categoria_contable.save()
    return categoria_contable

def verificar_modulos_necesarios():
    pass

def instalar_modulos_necesarios():
    pass

def configurar_modulos():
    pass
