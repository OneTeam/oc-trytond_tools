""" Configura un tryton con base a un pos de cafeteria y restaurante"""
import json
import os
import sys

from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from argparse import ArgumentParser

from .funciones import *

def merge_dict(main_dict: dict, complement_dict: dict)-> dict:
    """Busca las claves de main_dict en complement_dict y si la encuentra
    Reemplaza el valor de main_dict.
    """
    for mk in main_dict:
        if mk in complement_dict:
            main_dict[mk] = complement_dict[mk]
    return main_dict


def configurar_cafeteria(Model, Wizard, config, empresa_json: str = ''):
    """Configura una cafeteria
    con los datos de la empresa_cfg si se pasa (formato json)
    https://docs.python.org/3/library/configparser.html#supported-ini-file-structure.
    Espera Model y config pasado de proteus.
    """
    empresa = json.loads(empresa_json) or {} if empresa_json else {}
    EMPRESA = {
        'nombre': "Nombre Empresa",
        'telefonos': ['0000000'],
        'celulares': ['0000000000'],
        'identificador': '',
        'calle': '',
        'zona horaria': "America/Bogota",
        'listas de precios': ['lista1', 'lista2'],
        'tercero mostrador': 'Mostrador',
        'moneda': 'COP'
    }
    EMPRESA = merge_dict(EMPRESA, empresa)
    MODULOS_NECESARIOS = set([
        'currency',
        'country',
        'party',
        'company',
        'account',
        'trytonpsk_account_co_pyme',
        'product',
        'sale',
        'purchase',
        'sale_shop',
        'sale_payment',
        'sale_pos',
        'account_stock_continental',
        'one_click_for_sale',
        'one_click_for_purchase',
        'account_invoice_expenses',
        'sale_payment_form',
        'sale_w_tax',
        'sale_pos_extras',
        'product_kit',
        'stock_kit',
        'sale_kit'
    ])
    CUENTA_CAJA = "110505"
    CUENTA_BANCO = "11100501"
    CUENTA_A_COBRAR = "130505"
    CUENTA_A_PAGAR = "220505"
    CUENTA_COMPRAS_PADRE = "6135"
    CUENTA_COMPRAS = "613520"
    CUENTA_COMPRAS_NOMBRE = "Venta de productos en almacenes no especializados".upper()
    TIPO_CUENTA_COMPRAS = 'COSTO DE VENTAS Y OPERACIÓN'
    CUENTA_INGRESOS = "413520"
    CUENTA_PADRE_EXISTENCIAS = "1435"
    TIPO_PADRE_EXISTENCIAS = "UTILIDAD ANTES DE IMPUESTOS"
    CUENTA_EXISTENCIA = "143501"
    CUENTA_EXISTENCIA_COMPRAS = "143502"
    CUENTA_EXISTENCIA_COMPRAS_NOMBRE = "EXISTENCIAS COMPRAS"
    TIPO_CUENTA_EXISTENCIA_COMPRAS = "EXISTENCIAS COMPRAS"
    CUENTA_EXISTENCIA_VENTAS = "143503"
    CUENTA_EXISTENCIA_VENTAS_NOMBRE = "EXISTENCIAS VENTAS"
    TIPO_CUENTA_EXISTENCIA_VENTAS = "EXISTENCIAS VENTAS"
    CUENTA_EXISTENCIA_PERDIDO_NOMBRE = "EXISTENCIAS PERDIDO ENCONTRADO"
    CUENTA_EXISTENCIA_PERDIDO = "143504"
    TIPO_CUENTA_EXISTENCIA_PERDIDO = "EXISTENCIAS PERDIDO ENCONTRADO"
    CUENTA_PATRIMONIO_FACTURA_INICIAL = "311505"
    CATEGORIA_CONTABLE_PRODUCTOS = "CACHARRO"
    CATEGORIA_CONTABLE_GASTOS = "GASTOS"
    TIME_ZONE = EMPRESA.get('zona horaria', "America/Bogota")
    CURRENCY = EMPRESA.get('moneda', "COP")
    LISTAS_PRECIOS = EMPRESA.get('listas de precios', [])
    NOMBRE_TERCERO_MOSTRADOR = EMPRESA.get('tercero mostrador', 'Mostrador')
    CIFRAS_KILOGRAMO = 4
    #activar modulos
    activate_modules(MODULOS_NECESARIOS, Model, Wizard, config)
    Party = Model.get('party.party', config=config)
    Account = Model.get('account.account', config=config)
    Currency = Model.get('currency.currency', config=config)
    CuentaTipo = Model.get('account.account.type', config=config)
    ListaPrecio = Model.get('product.price_list', config=config)
    Uom = Model.get('product.uom', config=config)
    Journal = Model.get('account.journal', config=config)
    PaymentMethod = Model.get('account.invoice.payment.method', config=config)
    Sequence = Model.get('ir.sequence', config=config)
    Tienda = Model.get('sale.shop', config=config)
    Almacen = Model.get('stock.location', config=config)
    Usuario = Model.get('res.user', config=config)

    today = date.today()
    start_date = today + relativedelta(month=1, day=1)
    end_date = today + relativedelta(month=12, day=31)

    # crear tercero de empresa
    party = create_party(EMPRESA['nombre'], Model, config, EMPRESA['telefonos'],
                 EMPRESA['celulares'], EMPRESA['identificador'],
                 EMPRESA['calle'])
    # Moneda para empresa
    currency, = Currency.find([('code', '=', CURRENCY)])
    # agregar cambio a moneda para reportes de ventas
    currency.rates.new(date=start_date - timedelta(days=1),
           rate=Decimal(1.00))
    currency.save()

    # Configurar Empresa
    _ = create_company(party, currency, Model, Wizard, config)
    company = get_company(Model, config)
    # Agregar zona horaria a empresa
    company.timezone = TIME_ZONE
    company.save()
    # Crear año Fiscal
    fiscalyear = set_fiscalyear_invoice_sequences(
        create_fiscalyear(Model, config, company), Model, config)
    fiscalyear.click('create_period')
    # Crear plan de cuentas
    chart = create_chart(Wizard, Model, config, CUENTA_A_COBRAR, CUENTA_A_PAGAR, TIPO_CUENTA_COMPRAS, CUENTA_COMPRAS_NOMBRE, CUENTA_COMPRAS, CUENTA_COMPRAS_PADRE, CUENTA_INGRESOS, company=company, chart='trytonpsk_account_co_pyme.pc')
    # desactivar tercero requerido para cuenta caja general
    cuenta_caja, = Account.find([('code', '=', CUENTA_CAJA)])
    cuenta_caja.template_override = True
    cuenta_caja.party_required = False
    cuenta_caja.save()
    # desactivar tercero requerido para cuenta existencias
    cuenta_existencia, = Account.find([('company', '=', company.id),
                                       ('code', '=', CUENTA_EXISTENCIA)])
    cuenta_existencia.template_override = True
    cuenta_existencia.party_required = False
    cuenta_existencia.save()
    cuenta_compras, = Account.find([('code', '=', CUENTA_COMPRAS)])
    # Ajustar kilo
    kilogram, = Uom.find([('name', 'in', ('Kilogram', 'Kilogramo'))])
    kilogram.digits = CIFRAS_KILOGRAMO
    kilogram.rounding = 1.0 / (10 ** CIFRAS_KILOGRAMO)
    kilogram.save()
    # crear cuentas para inventario
    # tipo_existencia, = CuentaTipo.find(
    #     [('name', '=', 'INVENTARIOS')])
    tipo_existencia_perdido = crear_tipo_cuenta(
        nombre=TIPO_CUENTA_EXISTENCIA_PERDIDO,
        statement='income',
        nombre_padre=TIPO_PADRE_EXISTENCIAS,
        Model=Model, config=config,
        empresa=company, existencia=True)
    tipo_existencia_compra = crear_tipo_cuenta(
        nombre=TIPO_CUENTA_EXISTENCIA_COMPRAS,
        statement='income',
        nombre_padre=TIPO_PADRE_EXISTENCIAS,
        Model=Model, config=config,
        empresa=company, existencia=True)
    tipo_existencia_venta = crear_tipo_cuenta(
        nombre=TIPO_CUENTA_EXISTENCIA_VENTAS,
        statement='income',
        nombre_padre=TIPO_PADRE_EXISTENCIAS,
        Model=Model, config=config,
        empresa=company, existencia=True)
    cuenta_existencia_perdido = crear_cuenta(
        CUENTA_EXISTENCIA_PERDIDO_NOMBRE,
        CUENTA_EXISTENCIA_PERDIDO,
        tipo_existencia_perdido,
        CUENTA_PADRE_EXISTENCIAS,
        company, Model, config, en_balance=True
    )
    cuenta_existencia_venta = crear_cuenta(
        CUENTA_EXISTENCIA_VENTAS_NOMBRE,
        CUENTA_EXISTENCIA_VENTAS,
        tipo_existencia_venta,
        CUENTA_PADRE_EXISTENCIAS,
        company, Model, config, en_balance=True
    )
    cuenta_existencia_compra = crear_cuenta(
        CUENTA_EXISTENCIA_COMPRAS_NOMBRE,
        CUENTA_EXISTENCIA_COMPRAS,
        tipo_existencia_compra,
        CUENTA_PADRE_EXISTENCIAS,
        company, Model, config, en_balance=True
    )
    # crear categoria contable para producto
    categoria_contable_producto = crear_categoria_contable_producto(
        CATEGORIA_CONTABLE_PRODUCTOS,
        cuenta_compras,
        chart.form.category_account_revenue,
        cuenta_existencia,
        cuenta_existencia_venta,
        cuenta_existencia_compra,
        cuenta_existencia_perdido,
        Model, config)

    # crear listas precios
    for lista in LISTAS_PRECIOS:
        ListaPrecio(name=lista).save()

    # crear Metodos de pago Efectivo
    journal_cash, = Journal.find([('type', '=', 'cash')])
    payment_method = PaymentMethod()
    payment_method.name = 'Efectivo'
    payment_method.journal = journal_cash
    payment_method.credit_account = cuenta_caja
    payment_method.debit_account = cuenta_caja
    payment_method.save()

    # crear Metodos de pago Factura inicial
    cuenta_pago_patrimonio, = Account.find(
        [('code', '=', CUENTA_PATRIMONIO_FACTURA_INICIAL)])
    journal_cash, = Journal.find([('type', '=', 'cash')])
    payment_method = PaymentMethod()
    payment_method.name = 'Factura Inicial'
    payment_method.journal = journal_cash #@todo verificar si es este diario
    payment_method.credit_account = cuenta_pago_patrimonio
    payment_method.debit_account = cuenta_pago_patrimonio
    payment_method.save()

    # configurar almacen
    almacen, = Almacen.find([('name', 'in', ('Warehouse', 'Almacén'))])
    almacen.input_location = almacen.storage_location
    almacen.output_location = almacen.storage_location
    almacen.save()
    # configurar tienda
    mostrador = Party(name=NOMBRE_TERCERO_MOSTRADOR)
    mostrador.save()
    admin, = Usuario.find([('login', '=', 'admin')])
    tienda = Tienda(name="Principal", company=company,
                    warehouse=almacen)
    tienda.users.append(admin)
    tienda.party = mostrador
    tienda.self_pick_up = True
    tienda.save()

    # secuencias necesarias
    Sequence = Model.get('ir.sequence')
    secuencia_libro_ingresos, = Sequence.find([('name', '=', 'Libro Ingresos')])
    secuencia_libro_bancos, = Sequence.find([('name', '=', 'Libro Bancos')])
    secuencia_de_compra, = Sequence.find([('name', '=', 'Purchase')])

    # crear diarios tipo registros para diarios de pagos
    journal_pago_efectivo = Journal(
        name='Efectivo PTV', code='EPTV', active=True,
        type='statement', sequence=secuencia_libro_bancos)
    journal_pago_efectivo.save()

    journal_pago_banco = Journal(
        name='Banco PTV', code='BPTV', active=True,
        type='statement', sequence=secuencia_libro_bancos)
    journal_pago_banco.save()

    # cuenta banco
    cuenta_banco, = Account.find([('code', '=', CUENTA_BANCO)])
    cuenta_banco.template_override = True
    cuenta_banco.party_required = False
    cuenta_banco.save()

    #crear diarios de pagos
    StatementJournal = Model.get('account.statement.journal')
    diario_de_pago_efectivo = StatementJournal(
        name="Efectivo", journal=journal_pago_efectivo,
        account=cuenta_caja, validation='balance')
    diario_de_pago_efectivo.save()
    diario_de_pago_banco = StatementJournal(
        name="Banco", journal=journal_pago_banco, account=cuenta_banco,
        validation='balance')
    diario_de_pago_banco.save()

    #configurar ptv
    Device = Model.get('sale.device')
    device = Device(name='Principal')
    device.change_unit_price = True
    # error cuando agrega la tienda por primera vez
    try:
        device.shop = tienda
    except:
        pass
    device.shop = tienda
    device.journals.extend([diario_de_pago_banco, diario_de_pago_efectivo])
    device.journal = diario_de_pago_efectivo
    device.save()
    #configurar ptv a usuario administrador
    admin.shop = tienda
    admin.sale_device = device
    #ajustar idioma español al administrador
    Lang = Model.get('ir.lang')
    es, = Lang.find([('code', '=', 'es')])
    admin.language = es
    admin.save()

    # configurando destino de compras por defecto
    ConfPurchase = Model.get('purchase.configuration')
    conf_compra = ConfPurchase(1)
    conf_compra.purchase_sequence = secuencia_de_compra
    conf_compra.one_click_to_location = almacen.storage_location
    conf_compra.save()

    # configurando metodo de coste por defecto de productos
    ProductConf = Model.get('product.configuration')
    conf_producto = ProductConf(1)
    conf_producto.default_cost_price_method = 'average'
    conf_producto.save()

def main(database, config_file=None):
    try:
        from proteus import Model, config, Wizard
        from proteus.config import get_config
    except ImportError:
        prog = os.path.basename(sys.argv[0])
        sys.exit("proteus must be installed to use %s" % prog)

    config1 = config.set_trytond(database, config_file=config_file)
    configurar_cafeteria(Model, Wizard, config1, '')


def run():
    print("#################################")
    print("Recuerde Iniciar la base de datos")
    print("createdb base_datos")
    print("trytond-admin -c trytond.conf --all -d base_datos -l es")
    print("trytond-admin -c trytond.conf  -d base_datos -u country")
    print("python enlace_a_modules/country/scripts/import_countries.py -c trytond.conf -d base_datos")
    print("trytond-admin -c trytond.conf  -d auto -u currency")
    print("python enlace_a_modules/currency/scripts/import_currencies.py -c trytond.conf -d base_datos")
    print("#################################")


    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database')
    parser.add_argument('-c', '--config', dest='config_file',
                        help='the trytond config file')

    args = parser.parse_args()
    if not args.database:
        parser.error('Missing database')
    main(args.database, args.config_file)

if __name__ == '__main__':
    run()
